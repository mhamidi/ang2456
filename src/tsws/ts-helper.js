// npm install -g typescript
// npm upgrade -g typescript
// tsc -v
// template string and embedded expressions
var nblines = 1;
var astring = "this is a\nmultiline\nstring  " + (nblines + 1);
console.log(astring);
var numbers = [1, 2, 3];
// using a generic array
var nbs = [4, 5, 6];
// tuples
var tuple;
tuple = [1, 'a'];
tuple[4] = 'ab';
console.log(tuple[4]);
// Enums
var Car;
(function (Car) {
    Car[Car["VW"] = 2] = "VW";
    Car[Car["HONDA"] = 3] = "HONDA";
    Car[Car["BMW"] = 4] = "BMW";
})(Car || (Car = {}));
var car = Car.HONDA;
console.log('Car is :: ', Car[car], 'with index ', car);
// any type
var alist = [1, 'abc', 'fruit', true];
// variable declarations
// Array Destructuring
var mycars = [Car.VW, Car.HONDA];
var car1 = mycars[0], car2 = mycars[1];
console.log(car1, car2);
var _a = [1, 2, 3, 4], first = _a[0], rest = _a.slice(1);
console.log(first, rest);
// Object Destructuring
var obj = { a: 'abc', b: 15, c: false };
var b = obj.b, a = obj.a;
console.log('a :: ', a, '\nb :: ', b);
// Notice that we had to surround this statement with parentheses.
(_b = { a: 'baz', b: 101 }, a = _b.a, b = _b.b);
// Spread operator
var odds = [1, 3, 5];
var evens = [2, 4, 6];
var both_with_zero = [0].concat(odds, evens);
console.log('both_with_zero :: ', both_with_zero);
// interfaces
var numberz = [1, 2, 3];
var readonlyNbrs = [6, 7, 19];
numberz = readonlyNbrs;
console.log(numberz);
document.body.innerHTML = astring;
var _b;

// npm install -g typescript
// npm upgrade -g typescript
// tsc -v
// template string and embedded expressions
let nblines: number = 1;
let astring: string = `this is a
multiline
string  ${ nblines + 1 }`;
console.log(astring);

let numbers: number[] = [1, 2, 3];
// using a generic array
let nbs: Array<number> = [4, 5 , 6];

// tuples
let tuple: [number, string];
tuple = [1, 'a'];
tuple[4] = 'ab';
console.log(tuple[4]);

// Enums
enum Car {VW = 2, HONDA, BMW}
let car: Car = Car.HONDA;
console.log('Car is :: ', Car[car], 'with index ', car);

// any type
let alist: Array<any> = [1, 'abc', 'fruit', true];

// variable declarations

// Array Destructuring
let mycars = [Car.VW, Car.HONDA];
let [car1, car2] = mycars;
console.log(car1, car2);

let [first, ...rest] = [1, 2, 3, 4];
console.log(first, rest);

// Object Destructuring
let obj = {a: 'abc', b: 15, c: false};
let {b, a} = obj;
console.log('a :: ', a, '\nb :: ', b);
// Notice that we had to surround this statement with parentheses.
({ a, b } = { a: 'baz', b: 101 });

// Spread operator
let odds = [1, 3, 5];
let evens = [2, 4, 6];
let both_with_zero = [0, ...odds, ...evens];
console.log('both_with_zero :: ', both_with_zero);

// interfaces
let numberz: number[] = [1, 2, 3];
let readonlyNbrs: ReadonlyArray<number> = [6, 7, 19];
numberz = readonlyNbrs as Array<number>;
console.log(numberz);


document.body.innerHTML = astring;

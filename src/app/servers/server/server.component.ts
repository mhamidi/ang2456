import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};

  constructor(private serversService: ServersService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const serverid = +this.route.snapshot.params['serverid'];
    // when a crash occured, check if you parse your string into number
    this.server = this.serversService.getServer(serverid);
    this.route.params.subscribe(
      (params: Params) => {
        this.server = this.serversService.getServer(+params['serverid']);
      }
    );
  }

  onEdit() {
    // const serverid = +this.route.snapshot.params['serverid'];
    // this.router.navigate(['/servers', serverid, 'edit']);
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }

}

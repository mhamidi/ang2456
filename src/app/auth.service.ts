import { resolve, reject } from "q";

export class AuthService {
  loggedIn = false;

  isAuthenticated() {
    const promise = new Promise(
      (resolveIt, rejectIt) => {
        setTimeout(
          () => {
            resolve(this.loggedIn);
          }, 800
        );
      }
    );

    return promise;
  }

  login() {
    this.loggedIn = true;
  }

  logout() {
    this.loggedIn = false;
  }
}

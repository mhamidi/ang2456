import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, limit: number, endWith: string) {
    const endstr = endWith ? endWith : '';
    if (value.length > limit) {
      return value.substr(0, limit).concat(endstr);
    } else {
      return value;
    }
  }

}

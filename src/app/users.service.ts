import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class UsersService {

  constructor(private http: Http) { }

  storeUsers(users: any[]) {
    const requestHeaders = new Headers({'Content-Type': 'application/json'});
    return this.http.post('https://ws-http-backend.firebaseio.com/users.json',
      users,
      { headers: requestHeaders });
  }

  getUsers() {
    return this.http.get('https://ws-http-backend.firebaseio.com/users.json').map(
      (response: Response) => {
        const data = response.json();
        return data;
      }
    );
  }

  updateUsers(users: any[]) {
    const requestHeaders = new Headers({'Content-Type': 'application/json'});
    return this.http.put('https://ws-http-backend.firebaseio.com/users.json',
      users,
      { headers: requestHeaders });
  }

}

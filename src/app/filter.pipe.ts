import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: true
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string): any {
    if (value.length === 0 || !filterString) {
      return value;
    }
    const filterResult = [];
    value.forEach(element => {
      if (element[propName] === filterString) {
        filterResult.push(element);
      }
    });
    return filterResult;
  }

}
